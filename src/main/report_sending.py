from celery.schedules import crontab
from django.core.mail import EmailMessage

from config.celery_settings import app


def body_formatter(report):
    title = f'<h2>Краткий отчет за месяц:</h2>'
    first_str = f'<p>Доход:{report["Краткий отчет за месяц"]["Доход"]}</p>'
    second_str = f'<p>Расход:{report["Краткий отчет за месяц"]["Расход"]}</p>'
    third_str = f'<p>Расход:{report["Краткий отчет за месяц"]["Разница"]}</p>'
    return title + first_str + second_str + third_str


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    """Периодическая отправка на почту"""
    sender.add_periodic_task(
        crontab(hour=9, minute=13, day_of_month=12),
        send_report_on_email.s(),
    )


@app.task
def send_report_on_email(month: int, report: str):
    """Отправка отчета на почту"""
    message = EmailMessage(
        f'Отчет за {month} месяц',
        body_formatter(report),
        'nopro.ru@gmail.com',
        ['nopro.ru@gmail.com'],
    )
    message.content_subtype = 'html'
    message.attach_file('report_common.pdf')
    message.send()

