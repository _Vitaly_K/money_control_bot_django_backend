from django.urls import path
from . import api

urlpatterns = [
    path('create', api.CreateItemView.as_view()),
    path('all', api.RetrieveAllItemsView.as_view()),
    path('message_report', api.MessageReportView.as_view()),
    path('send_report', api.SendOnEmailReportView.as_view())
]