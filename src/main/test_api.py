import pytest
import json
import datetime
from django.conf import settings
from src.main.models import Item

with open(f'{settings.BASE_DIR}/test_data.json') as json_file:
    TEST_DATA = json.load(json_file)


@pytest.mark.django_db
def test_creating_item(client):
    create_test_item = client.post('/api/create', data=TEST_DATA[0])
    predicted_response = {
        'description': 'donation',
        'is_expense': False,
        'category': 'donation',
        'amount': 644
    }
    test_item = Item.objects.all()
    assert create_test_item.status_code == 201
    assert create_test_item.data == predicted_response
    assert test_item.exists()
    assert len(test_item) == 1


@pytest.mark.django_db
def test_getting_all_items(client):
    for test_item in TEST_DATA:
        client.post('/api/create', data=test_item)
    get_all_items = client.get('/api/all')
    test_item = Item.objects.all()
    assert get_all_items.status_code == 200
    assert len(test_item) == 11


@pytest.mark.django_db
def test_message_reporting(client):
    for test_item in TEST_DATA:
        client.post('/api/create', data=test_item)
    get_report = client.post('/api/message_report', data={'month': datetime.datetime.now().strftime('%m')})
    assert get_report.status_code == 200
    assert get_report.data['Краткий отчет за месяц'] == {
        'Доход': 36144,
        'Расход': 27400,
        'Разница': 8744
    }


@pytest.mark.django_db
def test_sending_report(client):
    for test_item in TEST_DATA:
        client.post('/api/create', data=test_item)
    get_report = client.post('/api/send_report', data={'month': datetime.datetime.now().strftime('%m')})
    assert get_report.data == 'Отчет отправлен на почту'
    assert get_report.status_code == 200